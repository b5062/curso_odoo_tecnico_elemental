
class FormularioRetencion(Model.models):
	_name = "formulario.retencion"
	_description = "Formulario de retención de Cobro de Documentos"
	_rec_name = "referencia"

	name = fields.Char(string="Formulario de Retención", default="RET-NEW")
	referencia = fields.Char(string="Número Documento a Cobrar")
	amount = fields.Float(string="Monto a Cobrar")
	porcentaje_retencion = fields.Float(string="Porcentaje de retención",default=0.03)
	monto_minimo_retencion = fields.Float(string="monto Mínimo retención",default=700.00)
	monto_retenido = fields.Float(string="monto Retenido",compute="_compute_monto_retenido",store=True,readonly=True)


	@api.depends('amount','porcentaje_retencion','monto_minimo_retencion')
	def _compute_monto_retenido(self):
		for rec in self:
			if rec.amount<monto_minimo_retencion:
				rec.monto_retenido = 0.00
			else:
				rec.monto_retenido = rec.amount*rec.porcentaje_retencion




