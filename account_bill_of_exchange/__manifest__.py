# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Letra de Cambio',
    'version' : '1.0',
    'summary': 'Emisión de Letras de Cambio de Cliente',
    'description': """Emisión de Letras de Cambio de Cliente""",
    'category': 'Accounting/Accounting',
    'depends' : ['base','account'],
    'data': [
        'security/ir.model.access.csv',
        'views/account_bill_exchange_view.xml',
        'views/account_bill_exchange_line_view.xml',
        'views/account_move_view.xml',
        'report/report_paperformat.xml',
        'report/report.xml',
        'report/account_bill_exchange_report.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'LGPL-3',
}