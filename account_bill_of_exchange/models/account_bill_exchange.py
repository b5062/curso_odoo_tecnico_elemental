# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

class AccountBillExchange(models.Model):
	_name='account.bill.exchange'
	_description='Letra de cambio de Cliente'

	###############################
	state = fields.Selection(selection=[('draft','Borrador'),('generated','Generado')],
		readonly=True, states={'draft': [('readonly', False)]},
		string="Estado",default="draft")

	company_id = fields.Many2one('res.company',
		string="Compañia", 
		default=lambda self: self.env['res.company']._company_default_get('account.invoice'),
		domain = lambda self: [('id', 'in',[i.id for i in self.env['res.users'].browse(self.env.user.id).company_ids] )],
		readonly=True)

	########################################
	date_emission = fields.Date(string="Fecha de emisión")
	date_maturity = fields.Date(string="Fecha de Vencimiento")

	currency_id = fields.Many2one('res.currency',string="Moneda") ## campo mandatorio

	amount = fields.Float(string="Monto total",compute="compute_campo_amount",currency_field='currency_id')

	street_operation = fields.Char(string="Lugar de Giro")
	reference_girador = fields.Char(string="Referencia de Girador")
	document_number = fields.Char(string="N° Letra")

	###### Fields Receptor #######
	partner_id = fields.Many2one('res.partner',string="Cliente")

	##### 1 -> * ### DETALLE DE LAS FACTURAS APLICADAS.
	account_bill_exchange_line_ids = fields.One2many('account.bill.exchange.line',
		'bill_exchange_id',string="Detalle de Facturas a Aplicar")


	@api.depends('account_bill_exchange_line_ids','account_bill_exchange_line_ids.monto_aplicar')
	def compute_campo_amount(self):
		for rec in self:
			rec.amount = 0.00
			if rec.account_bill_exchange_line_ids:
				rec.amount = sum([i.monto_aplicar for i in rec.account_bill_exchange_line_ids])


	def action_generated(self):
		if self.account_bill_exchange_line_ids:
			for line in self.account_bill_exchange_line_ids:
				line.saldo_residual = line.monto_total - line.monto_aplicar

				#### Escribiendo sobre cada asiento:
				line.invoice_id.write({'bill_exchange_id':self.id})

			self.state = 'generated'



	



