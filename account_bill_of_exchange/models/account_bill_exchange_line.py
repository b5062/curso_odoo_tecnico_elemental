from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

class AccountBillExchangeLine(models.Model):
	_name='account.bill.exchange.line'
	_description='Detalles Letra de cambio de Cliente'

	bill_exchange_id = fields.Many2one('account.bill.exchange',string="Letra de Cambio",ondelete="cascade")

	invoice_id = fields.Many2one('account.move',string="Factura de Cliente")

	#### CAMPOS COMPUTADOS ########
	currency_id = fields.Many2one('res.currency',string="Moneda del Documento",compute="compute_currency_id")

	monto_total = fields.Float(string="Monto Total Factura",compute="compute_monto_total")

	invoice_date = fields.Date(string="Fecha Emisión Factura",compute="compute_invoice_date")

	saldo_residual = fields.Float(string="Saldo Pendiente",compute="compute_saldo_residual")
	###############################
	monto_aplicar = fields.Float(string="Monto a Aplicar")

	@api.depends('invoice_id')
	def compute_currency_id(self):
		for rec in self:
			rec.currency_id = False
			if rec.invoice_id:
				rec.currency_id = rec.invoice_id.currency_id

	@api.depends('invoice_id')
	def compute_monto_total(self):
		for rec in self:
			rec.monto_total=0.00
			if rec.invoice_id:
				rec.monto_total = rec.invoice_id.amount_total


	@api.depends('invoice_id')
	def compute_invoice_date(self):
		for rec in self:
			rec.invoice_date = False
			if rec.invoice_id and rec.invoice_id.invoice_date:
				rec.invoice_date = rec.invoice_id.invoice_date

	@api.depends('invoice_id')
	def compute_saldo_residual(self):
		for rec in self:
			rec.saldo_residual = 0.00
			if rec.invoice_id:
				rec.saldo_residual = rec.invoice_id.amount_residual


