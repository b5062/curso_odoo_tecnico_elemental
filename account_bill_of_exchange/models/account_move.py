# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

class AccountMove(models.Model):
	_inherit = 'account.move'

	bill_exchange_id = fields.Many2one('account.bill.exchange',string="Letra de Cambio",readonly=True)